/**
 * Interface for working with BE data
 */

// Carbon Interface only supports Canada (ca) and the US (us).
export type Country = 'us' | 'ca'
export type ElectricityUnit = 'mwh' | 'kwh'

export interface EstimateData {
    value: number;
    country: Country;
    state?: string;
    unit?: ElectricityUnit,
}

// interface for raw BE data
export interface EstimateResponseDirty {
    data: {
        id: string,
        type: 'estimate',
        attributes: {
            country: Country,
            state?: string,
            electricity_unit: ElectricityUnit,
            electricity_value: number,
            estimated_at: string,
            cargon_g: 18051428;
            carbon_lb: number;
            carbon_kg: number;
            carbon_mt: number;
        }
    }
}

// interface for FE
export interface EstimateResponse {
    id: string,
    type: 'estimate',
    country: Country,
    state?: string,
    unit: ElectricityUnit,
    value: number,
    estimatedAt: Date,
    carbon: {
        g: number,
        lb: number,
        kg: number,
        mt: number,
    }
}