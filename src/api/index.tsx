import { API_URLS, AUTH_KEY } from 'settings/env'
import { Country, ElectricityUnit, EstimateResponse, EstimateResponseDirty } from './types';
import { get, post } from './fetch'

// payload to create an estimation
interface EstimateData {
    value: number;
    country: Country;
    state?: string;
    unit?: ElectricityUnit,
}

// payload to create a weekly estimation (multiple days)
interface EstimateWeeklyData {
    values: number[];
    country: Country;
    state?: string;
    unit?: ElectricityUnit,
}


// map BE interface to FE interface
const mapEstimateResponse = ( {
    data: {
        id,
        type,
        attributes: {
            country,
            state,
            electricity_unit: unit,
            electricity_value: value,
            estimated_at: estimatedAt,
            cargon_g: g,
            carbon_kg: kg,
            carbon_mt: mt,
            carbon_lb: lb,
        }
    }
}: EstimateResponseDirty ) : EstimateResponse => ( {
    id,
    type,
    country,
    state,
    unit,
    value,
    estimatedAt: new Date( estimatedAt ),
    carbon: {
        g,
        lb,
        kg,
        mt,
    }
} )

// will do the request to create the estimate
export const createCarbonEstimate = ( { value, country, state, unit = 'mwh' }: EstimateData, authKey = AUTH_KEY ): Promise<EstimateResponse> => {

    const reqBody = {
        type: 'electricity',
        electricity_unit: unit,
        electricity_value: value,
        country,
        state,
    }

    return new Promise( ( resolve, reject ) => { 
        return post<EstimateResponseDirty>( API_URLS.CREATE_ESTIMATE, reqBody, {
            headers: {
                'Authorization': `Bearer ${authKey}`,
            },
        } )
            .then( estimate => resolve( mapEstimateResponse( estimate ) ) )
            .catch( reject )
    } )
}

// will create multiple estimates by an array of values
export const createWeeklyCarbonEstimate = ( { values, country, state, unit = 'mwh' } : EstimateWeeklyData ) => {
    return Promise.all(
        values.map( value => createCarbonEstimate( {
            value,
            country,
            state,
            unit,
        } ) )
    )
}

// will get estimate by id
export const getCarbonEstimate = ( id: string, authKey = AUTH_KEY ): Promise<EstimateResponse> => {
    return new Promise( ( resolve, reject ) => {
        console.log( API_URLS.GET_ESTIMATE( id ) )
        return get<EstimateResponseDirty>( API_URLS.GET_ESTIMATE( id ), {
            headers: {
                'Authorization': `Bearer ${authKey}`,
            },
        } )
        .then( estimate => resolve( mapEstimateResponse( estimate ) ) )
        .catch( reject )
    } )
}
