import { createCarbonEstimate, getCarbonEstimate  } from './index'

test( `check if 'createCarbonEstimate' will return the status in case of bad auth key`, async () => {
    try {
        await createCarbonEstimate( {
            value: 3,
            country: 'us',
        }, '' )
    } catch( e ) {
        // eslint-disable-next-line jest/no-conditional-expect
        expect( e.status )
            .toBe( 401 )
    }
} )

test( `check if 'createCarbonEstimate' creates an estimate`, async () => {
    const { id: createdId } = await createCarbonEstimate( {
        value: 3,
        country: 'us',
    } )

    const {
        id: getId
    } = await getCarbonEstimate( createdId )

    expect( createdId )
        .toBe( getId )

} )