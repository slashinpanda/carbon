import fetchBase from 'isomorphic-fetch' // since test will run in NodeJS runtime, fetch will be undefined, isomorphic-fetch will provide the same API for Browser and NodeJS

// wrapper around fetch to handle response body based on content-type
// and return status code in case of a rejetion
export const fetch = <T>( url: string, options: RequestInit ): Promise<T> => {
    return new Promise( ( resolve, reject ) => {
        // will be used to reject or resolve the promise
        let okay: boolean;
        // status will be returned in case of a rejection
        let status: number;
        return fetchBase( url, options )
        .then( resp => {
            status = resp.status
            okay = resp.ok
            // in case BE will return text for 4XX,5XX statuses
            const isJSon = (resp.headers.get('content-type') || '' ).includes( 'application/json' )
            return ( isJSon ? resp.json() : resp.text() ) as Promise<T>
        } )
        .then( body => {
            return okay? resolve( body ) : reject( {
                status,
                body,
            } )
        } )
        // something unexpected
        .catch( reject )
    } )
}

export const post = <T>( url: string, body: any, options: RequestInit ) =>
    fetch<T>( url, {
        ...options,
        method: 'POST',
        headers: {
            ...options.headers,
            'Content-Type': 'application/json',
        },
        body: JSON.stringify( body )
    } )

export const get = <T>( url: string, options: RequestInit ) =>
    fetch<T>( url, {
        ...options,
        method: 'GET',
    } )
