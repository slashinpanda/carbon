const Host = "https://www.carboninterface.com"

export const {
    REACT_APP_API_KEY: AUTH_KEY = ''
} = process.env

export const API_URLS = {
    CREATE_ESTIMATE: `${ Host }/api/v1/estimates`,
    GET_ESTIMATE: ( estimateId: string ) => `${ Host }/api/v1/estimates/${ estimateId }`,
}