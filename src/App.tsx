import * as React from 'react'
import './App.css';
import { Container, Grid } from '@material-ui/core'
import EstimateForm, { EstimateFormData } from 'components/EstimateForm';
import ChartLoader from 'components/Chart/loader';

function App() {

  const [ formData, setFormData ] = React.useState<EstimateFormData>()

  return (
    <Container maxWidth='lg'>
      <Grid container>
        <Grid item xs={12} md={4}>
          <EstimateForm
            onSubmit={ setFormData }
          />
        </Grid>
        <Grid item xs={12} md={8}>
          { formData ? (
            <ChartLoader formData={ formData } />
          ) : null }
        </Grid>
      </Grid>
    </Container>
  );
}

export default App;
