import * as React from 'react'

interface Props {
    fallback: React.ReactNode,
    children: React.ReactNode
}

interface State {
    hasError: boolean;
    error: Error | null;
}

// works only with React.Component/PureComponent class
// will display a fallback for unhandled rejections
export class ErrorBoundary extends React.PureComponent<Props,State> {

    state = {
        hasError: false,
        error: null
    }

    static getDerivedStateFromError(error: Error) {
        return {
            hasError: true,
            error
        };
    }

    render() {
        if (this.state.hasError) {
            return this.props.fallback;
        }
            return this.props.children;
    }
    
}