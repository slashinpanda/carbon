import * as React from 'react'
import { EstimateResponse } from 'api/types'
import { ResponsiveLine } from '@nivo/line'

export interface ChartData {
    key: string,
    value: number,
}

interface Props {
    estimates: EstimateResponse[],
    startDate: Date,
}

// interface used by nivo chart
interface ChartLine {
    id: string,
    color: string,
    data: {
        x: string,
        y: number,
    }[]
}

// height of the chart
const Height = 800

const Chart: React.FC<Props> = ( { estimates, startDate } ) => {

    const data = React.useMemo( () => {
        // create points for the char
        // x - date string, e.g: 31 jan
        // y - carbon, e.g: .7mt
        const line = estimates.reduce( ( acc, { carbon: { mt: y } }, i ) => {
            const date = new Date( startDate )
            date.setDate( date.getDate() + i )
            acc.data.push( {
                x: date.toLocaleDateString( 'en-US', {
                    month: 'short',
                    day: 'numeric',
                } ),
                y,
            } )
            return acc
        }, {
            id: 'week',
            color: "hsl(22, 70%, 50%)",
            data: [],
        } as ChartLine )

        return [ line ]
    }, [ estimates, startDate ] )

    return (
        <div style={ {
            height: Height,
        } }>
            <ResponsiveLine
                margin={ { top: 50, right: 60, bottom: 50, left: 60 } }
                xScale={ { type: 'point' } }
                yScale={ { type: 'linear', min: 'auto', max: 'auto', stacked: true, reverse: false } }
                yFormat=" >-.2f"
                axisTop={ null }
                axisRight={ null }
                axisLeft={ {
                    orient: 'left',
                    tickSize: 5,
                    tickPadding: 5,
                    tickRotation: 0,
                    legend: 'CO2(mt)',
                    legendOffset: -50,
                    legendPosition: 'middle'
                } }
                pointSize={10}
                pointColor={ { theme: 'background' } }
                pointBorderWidth={ 2 }
                pointBorderColor={ { from: 'serieColor' } }
                pointLabelYOffset={ -12 }
                data={ data }
            />
        </div>

    )

}

export default React.memo( Chart )