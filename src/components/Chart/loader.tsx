import { Box, Container, withStyles } from '@material-ui/core'
import { Alert, AlertTitle } from '@material-ui/lab';
import { createWeeklyCarbonEstimate } from 'api'
import { ErrorBoundary } from 'components/ErrorBoundary'
import { EstimateFormData } from 'components/EstimateForm'
import Progress from 'components/Progress'
import * as React from 'react'

const ChartContainer = withStyles( {
    root: {
        position: 'relative' as const,
        height: '100%',
    }
} )( Container )

// lazy load chart component and query estimates in parallel
const loadChart =  ( {
    date,
    values,
    country,
}: EstimateFormData ) => React.lazy( () => {
    return Promise.all( [
        import( './index' ),
        createWeeklyCarbonEstimate( {
            values: values!,
            country: country!,
        } )
    ] )
    .then( ( [ { default: Component }, estimates ] ) => {
        return {
            default: React.memo( () => {
                return (
                    <Component startDate={ new Date( date! ) } estimates={ estimates } />
                )
            } )
        }
    } )
    .catch( Promise.reject )
} )

// in case of a rejection, an error will be displayed
const Error = React.memo( () => {
    return (
        <Box m={4}>
            <Alert severity="error">
                <AlertTitle>Failed to estimate emisisons</AlertTitle>
                Please check if you used a valid API Key
            </Alert>
        </Box>
    )
} )

// display a progress until chart is loaded
const ChartLoader: React.FC<{
    formData: EstimateFormData
}> = ( { formData } ) => {

    const Loader = React.useMemo( () => {
        return loadChart( formData )
    }, [ formData ] )

    return (
        <ChartContainer>
            <ErrorBoundary fallback={<Error />}>
                <React.Suspense fallback={ <Progress /> }>
                    <Loader />
                </React.Suspense>
            </ErrorBoundary>
        </ChartContainer>
    )
}

export default React.memo( ChartLoader )