import { CircularProgress, WithStyles, withStyles } from '@material-ui/core'
import * as React from 'react'
import clsx from 'clsx'

const styles = {
    root: {
        position: 'absolute' as const,
        top: '50%',
        left: '50%',
        marignLeft: '-25px',
        marignTop: '-25px',
    }
}

interface Props extends WithStyles<typeof styles> {
    children?: React.ReactNode;
    className?: string;
}

const Progress: React.FC<Props> = ( { classes, className } ) => {

    return (
        <CircularProgress className={ clsx( classes.root, className ) } />
    )

}


export default React.memo( withStyles( styles )( Progress ) )

