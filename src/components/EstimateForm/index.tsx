import * as React from 'react'
import { FormControl, Box, InputLabel, MenuItem, Paper, Select, TextField, Button, Typography } from '@material-ui/core'
import { Controller, useForm } from 'react-hook-form'
import { Country } from 'api/types';
import * as yup from 'yup'
import { yupResolver } from '@hookform/resolvers/yup'

// validation
const schema = yup.object().shape( {
    country: yup.string().required(),
    values: yup.array().label('').of( yup
        .number()
        .typeError( 'must be a number' )
        .positive( 'must be a positive number' )
        .required()
    )
        .required(),
} )

// form data
export interface EstimateFormData {
    country?: Country,
    values?: number[],
    date?: string,
}

interface Props {
    onSubmit: ( data: EstimateFormData ) => void;
}

const EstimateForm: React.FC<Props> = ( { onSubmit } ) => {

    const { control, handleSubmit, formState: { errors }, watch } = useForm<EstimateFormData>( {
        resolver: yupResolver( schema )
    } )

    // start date
    const date = watch( 'date' )

    // will fill the dates based on the selected start date
    const days = React.useMemo( () => {
        if ( !date ) {
            return []
        }
        const startDate = new Date( date )
        return new Array( 7 )
            .fill( null )
            .map( ( _, i ) => {
                const date = new Date( startDate )
                date.setDate( date.getDate() + i )
                return date.toLocaleDateString( 'en-US', {
                    weekday: 'short',
                    month: 'short',
                    day: 'numeric',
                } )
            } )
    }, [ date ] )

    return (
        <form onSubmit={ handleSubmit( onSubmit ) }>
            <Paper>
                <Box m={2} mb={4}>
                    <FormControl margin='normal' fullWidth>
                        <Typography variant='h6'>Estimate emissions</Typography>
                    </FormControl>
                    <FormControl margin='normal' fullWidth>
                        <InputLabel required id="country">Country</InputLabel>
                        <Controller
                            name='country'
                            control={ control }
                            defaultValue=""
                            render={ ( { field } ) => (
                                <Select { ...field } required error={ Boolean( errors.country ) } labelId="country">
                                    <MenuItem value='us'>United States</MenuItem>
                                    <MenuItem value='ca'>Canada</MenuItem>
                                </Select>
                            ) }
                        />
                    </FormControl>
                    <FormControl margin='normal' fullWidth>
                        <Controller
                            name='date'
                            control={ control }
                            defaultValue=""
                            render={ ( { field } ) => (
                                <TextField
                                    { ...field }
                                    id="date"
                                    label="Start date"
                                    type="date"
                                    defaultValue="2020-04-04"
                                    InputLabelProps={{
                                        shrink: true,
                                    }}
                                /> 
                            ) }
                        />
                    </FormControl>
                    { days.map( ( day, index ) => (
                        <FormControl margin='normal' fullWidth key={ day }>
                            <Controller
                                name={ `values.${index}` as 'values' }
                                control={ control }
                                defaultValue=""
                                render={ ( { field } ) => <TextField
                                    { ...field }
                                    error={ Boolean( errors.values && errors.values[index] ) }
                                    helperText={ errors.values && errors.values[index] && errors.values[index]!.message }
                                    required
                                    label={ `${day}: Electricity Consumption (mwh)` }
                                    type="numeric" />
                                }
                            />
                        </FormControl>
                    ) ) }
                    <FormControl margin='normal' fullWidth>
                        <Box mb={4}>
                            <Button type='submit' variant="contained" color="primary">
                                Estimate
                            </Button>
                        </Box>
                    </FormControl>
                </Box>
            </Paper>
        </form>
    )

}

export default React.memo( EstimateForm )